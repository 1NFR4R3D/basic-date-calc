#include <stdio.h>
#include <stdlib.h>

int day, month, year, outDay, outMonth, outYear, n, monthArray[]={31,28,31,30,31,30,31,31,30,31,30,31};

int checkLeap(int a){
	/*Check if year passed is a leap yea, return 0 if it is not, 1 if it is*/
	if(a%4 == 0)
	{
		if(a%100 == 0)
		{
			if (a%400 == 0){
				return 1;
			}
			else{
				return 0;
			}
		}
		else{
			return 1;
		}
	}
	else{
	return 0;
	}
}

int input(){
	/*Get input from user and check if it is valid. Return 1 if invalid, 0 otherwise*/
	printf("Enter date -\n");
	scanf("%d %d %d",&day,&month,&year);
	if(day>monthArray[month-1] +checkLeap(year)){
		printf("Error! Invalid day!");
		return 1;
	}
	printf("%d/%d/%d\n",day,month,year);
	printf("Enter number of days to be added - ");
	scanf("%d",&n);
	return 0;
}

int addToDate(){
	/*Add number of days to input and store in a different set of variables.
	Also adjust for months and years*/
	outMonth=month;
	outYear=year;
	outDay=day+n;
	for(;outDay>monthArray[outMonth-1];++outMonth){
		if(outMonth==3&&checkLeap(outYear)&&outDay==1){
			outDay=29;
			outMonth--;
		}
		outDay=outDay-monthArray[outMonth-1];
		if(outMonth==2&&checkLeap(outYear)){
			outDay--;
		}
		if(outMonth>12){
			outYear++;
			outMonth=0;
		}
	}
	if(outDay==0){
		outDay=29;
		outMonth=2;
	}
}

void output(){
	/*Print output*/
	printf("%d/%d/%d + %d days = %d/%d/%d",day,month,year,n,outDay,outMonth,outYear);
}

int main(){
	if(input()){
		goto error;
	}
	addToDate();
	output();
error:
	return 0;
}
